import React, { Component } from 'react';
import videosrc from '../sample.mp4'

class VideoPlayer extends Component {
    constructor(props){
        super(props)
        this.state={className:'q4',pause:true}
        this.movePlayer=this.movePlayer.bind(this)
    }
    movePlayer=(event)=>{
        this.refs.vPlayer.pause()
        console.log(event)
        let width=window.screen.width;
        let height=window.screen.height;
        console.log('width/2',width/2)
        console.log('height/2',height/2)
        console.log('dragH',event.pageY)
        console.log('dragW',event.pageX)
        if(event.pageX < width/2){
            if(event.pageY < height/2){
                this.setState({className:'q1'})
            }
            else{
                this.setState({className:'q3'})
            }
        }
        else if(event.pageX > width/2){
            if(event.pageY < height/2){
                this.setState({className:'q2'})
            }
            else{
                this.setState({className:'q4'})
            }
            
        }
    }
    render() {
        return (
            <video ref="vPlayer" width="200" height="300" controls={true} draggable={true} className={'videoPlayer '+ this.state.className} onDragEnd={this.movePlayer}>
                <source src={videosrc} type="video/mp4"/>
                    your browser doesn't support video
                </video>
            
        );
    }
}

export default VideoPlayer;