import logo from './logo.svg';
import './App.css';
import VideoPlayer from './component/videoPlayer';

function App() {
  return (
    <div className="App">
      <h2>Drag video to place it on the corner of any quarter</h2>
      <VideoPlayer/>
    </div>
  );
}

export default App;
